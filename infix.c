#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <stdlib.h>
#include "cstack.h"
#include "stack.h"
#include "token.h"

int readline(char *arr, int len) {
	int i = 1;
	int ch;
	arr[0] = '(';
	while((ch = getchar()) != '\n' && i < len - 1) {
		if(ch != ' '){		
			arr[i] = ch;
			i++;
		}
	}
	arr[i] = ')';
	i++;
	arr[i] = '\0';
	return i;
}

int precedence(char op) {
	if(op == '%')
		return 3;
	if(op == '*' || op == '/')
		return 2;
	if(op == '+' || op == '-')
		return 1;
	return 0;
}

char ctop(cstack *cs) {
	char x = cpop(cs);				
	cpush(cs, x); // the error was due to passing invalid argument			
	return x;
}

int infix(char *str) {
	stack result;
	cstack cs;
	token *t;
	char temp[16];
	int reset = 1, a, b, x, y, num;
	char ch;

	init(&result);
	cinit(&cs);
	while(1) {
		t = getnext (str, &reset);
		if(t->type == OPERAND) {
				push(&result, t->number);			
		}
		else if (t->type == OPERATOR) {
printf("si = %d\n", cempty(&cs));
			if(!cempty(&cs)) {
				ch = ctop(&cs);	
				a = precedence(ch);	
				b = precedence(t->op);	
				while(a >= b && !cempty(&cs)) {
					ch = cpop(&cs);
					x = pop(&result);
					y = pop(&result);
					switch(ch) {
						case '+':
							num = y + x;
							break;
						case '-':
							num = y - x;
							break;
						case '*':
							num = y * x;
							break;
						case '/':
							if(x == 0)
								return INT_MIN;
							else
								num = y / x;
							break;
						case '%':
							num = y % x;
							break;
				
					}
					push(&result, num);
					ch = ctop(&cs);	
					a = precedence(ch);
				}
			}
			cpush(&cs, t->op);
		} else if (t->type == END) {
			while(!cempty(&cs)) {
				a = cpop(&cs);
				x = pop(&result);
				y = pop(&result);
				switch(a) {
					case '+':
						num = y + x;
						break;
					case '-':
						num = y - x;
						break;
					case '*':
						num = y * x;
						break;
					case '/':
						if(x == 0) // there was problem in assignment symbol			
							return INT_MIN;
						else
							num = y / x;
						break;
					case '%':
						num = y % x;
						break;			
				}
				push(&result, num);
			}
			return result.a[0];
		} 
		
	}
}

int solve(char *str){
	char ch, s[128], s1[64], temp[1];
	cstack cs;
	int result, i = 0, k, j, n;
	cinit(&cs);
	while(str[i] != '\0'){
		while(str[i] != ')'){
			cpush(&cs, str[i]);
			i++;
		}
		k = i - 1;
		i++;
		j = 0;
		while((s[j] = cpop(&cs)) != '('){
			j++;
		}
		for(n = 0, k = j -1; n < ((j + 1) / 2 ); n++, k--){
			ch = s[n];
			s[n] = s[k];
			s[k] = ch;
		}
		s[j] = '\0';
		result = infix(s);
		if(str[i] != '\0'){
			k = result;
			j = 0;
			while(k != 0){
				n = k % 10;
				k = k / 10;
				s1[j] = n + '0';
				j++;
			}
			j--;
			while(j != -1){
				cpush(&cs, s1[j]);
				j--;
			}
		}
	}
	return result;
}

int main() {
	char str[128];
	int x, ans;
	char *p;
	while((x = readline(str, 128))) {
		ans = solve(str);
		if(ans == INT_MIN) {
			fprintf(stderr, "Erorr in expression\n");
		}  else
			fprintf(stdout, "%d\n", ans);
	}
	return 0;
}
